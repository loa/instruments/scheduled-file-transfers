:: ------------------------------------------------------------------------
:: NAME     : install_uploader.bat
:: AUTHOR   : Romain De Filippi - LOA
:: EMAIL    : romain.defilippi@univ-lille.fr
:: DATE     : 2022-06-15
::
:: COMMENTS : This script create directories and download WinSCP binaries
::            and scripts for uploading files to LOA FTP server.
::            PLEASE, DO NOT EDIT THIS SCRIPT !
:: ------------------------------------------------------------------------

:: Environment
@echo off
setlocal EnableDelayedExpansion

:: Variables
set URL_REPO=https://gitlab.univ-lille.fr/loa/instruments/scheduled-file-transfers/-/raw/master
set PATH_LOA="C:\LOA"

:: Create directories
set directories=\ \auth \bin \conf \logs \tmp
for %%d in (%directories%) do (
  :: Create directories if not exist
  if not exist %PATH_LOA%%%d (
    echo Creating the directory %PATH_LOA%%%d
    md %PATH_LOA%%%d >NUL 2>&1
	:: Test the md command result
    if %ERRORLEVEL% NEQ 0 (
      echo [ERROR] Unable to create '%PATH_LOA%%%d' ^^!
	  goto error
    )
  )
)

:binaries
:: Download the uploader script and WinSCP binaries
echo.
for %%b in (uploader.bat _library.bat WinSCP.com WinSCP.exe WinSCPnet.dll) do (
  echo Downloading '%%b' from the repository ...
  call :download %%b bin %PATH_LOA%
  if not exist "%PATH_LOA%\bin\%%b" (
    echo [ERROR] Unable to find or download '%%b' from the repository ^^!
    goto error
  )
)

:execute
echo.
set /p launch=Execute the script [Y/n] ? 
if /i "%launch%" == "" goto uploader
if /i "%launch%" == "N" goto quit
if /i "%launch%" == "Y" (
  :uploader
  if exist %PATH_LOA%\bin\uploader.bat call %PATH_LOA%\bin\uploader.bat
  goto quit
)
goto execute

:download
:: Trying to download the script.extension (script if exists, else a html page)
curl "%URL_REPO%/%~2/%~1" -o %~3\%~2\%~1 >NUL 2>&1

:: Read the downloaded file
set /p file=<%~3\%~2\%~1

:: Test the content of downloaded file (GitLab webpage for not found file)
if "!file:~10,4!" == "html" (
  del %~3\%~2\%~1 >NUL 2>&1
  exit /b 1
)
exit /b 0

:error
timeout /t 5 /nobreak
exit /b 1

:quit
timeout /t 5 /nobreak
exit /b 0
