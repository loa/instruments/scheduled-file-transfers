:: CODENAME
set CODENAME=SkyImager CMS

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30000

:: SCHEDULED TASK
set START_TIME=06
set EVERY_MIN=10

:: FILES
:: l2410409.094567
set DATA[0].SendMethod=sync
set DATA[0].DataPath="D:\ProgramData\cloudcam\ccam_01065"
set DATA[0].ArchivesPath=
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=*20*
set DATA[0].ArchiveFormat=
set DATA[0].ArchiveCurrDay=
set DATA[0].YearPos=
set DATA[0].YearLen=
set DATA[0].MonthPos=
set DATA[0].MonthLen=
set DATA[0].DayPos=
set DATA[0].DayLen=
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
