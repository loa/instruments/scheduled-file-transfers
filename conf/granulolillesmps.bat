:: CODENAME
set CODENAME=SMPS

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30

:: SCHEDULED TASK
set START_TIME=04
set EVERY_MIN=15

:: FILES
:: SMPS_3082001718003_20220614_065100.csv
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\Users\LOA\TSI\Aerosol Instrument Manager SMPS\AutoExport"
set DATA[0].ArchivesPath="C:\Users\LOA\TSI\Aerosol Instrument Manager SMPS\AutoExport\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=SMPS_3082001718003_*.csv
set DATA[0].ArchiveFormat=YYYYMM
set DATA[0].ArchiveCurrDay=0
set DATA[0].YearPos=19
set DATA[0].YearLen=4
set DATA[0].MonthPos=23
set DATA[0].MonthLen=2
set DATA[0].DayPos=25
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
