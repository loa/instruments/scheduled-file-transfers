:: CODENAME
set CODENAME=Radiometer IRT

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=300

:: SCHEDULED TASK
set START_TIME=07
set EVERY_MIN=15

:: FILES
:: AMX_02241500.K7
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\RadiometerTIR\AgiaMarina"
set DATA[0].ArchivesPath="C:\RadiometerTIR\AgiaMarina\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=*.K7
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=13
set DATA[0].YearLen=2
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=8
set DATA[0].HourLen=2
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

:: AMX_1424022025.R4M
set DATA[1].SendMethod=put
set DATA[1].DataPath="C:\RadiometerTIR\AgiaMarina"
set DATA[1].ArchivesPath="C:\RadiometerTIR\AgiaMarina\LOA"
set DATA[1].RemotePath="/home/"
set DATA[1].ToRename=
set DATA[1].Mask=AMX*
set DATA[1].ArchiveFormat=YYYYMMDD
set DATA[1].ArchiveCurrDay=1
set DATA[1].YearPos=10
set DATA[1].YearLen=4
set DATA[1].MonthPos=8
set DATA[1].MonthLen=2
set DATA[1].DayPos=6
set DATA[1].DayLen=2
set DATA[1].HourPos=4
set DATA[1].HourLen=2
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=