:: CODENAME
set CODENAME=Radiometer MO

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30000

:: SCHEDULED TASK
set START_TIME=09
set EVERY_MIN=10
for /f "tokens=3 delims=/" %%A in ("%DATE%") do set YEAR=%%A

:: FILES
:: *
set DATA[0].SendMethod=sync
set DATA[0].DataPath="C:\RPG-HATPRO\DATA\Y%YEAR%"
set DATA[0].ArchivesPath=
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=2*
set DATA[0].ArchiveFormat=
set DATA[0].ArchiveCurrDay=
set DATA[0].YearPos=
set DATA[0].YearLen=
set DATA[0].MonthPos=
set DATA[0].MonthLen=
set DATA[0].DayPos=
set DATA[0].DayLen=
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
