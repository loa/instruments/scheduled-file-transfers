:: CODENAME
set CODENAME=Lidar BOLIVIE

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30000

:: SCHEDULED TASK
set START_TIME=02
set EVERY_MIN=10

:: FILES
:: 20250113_1550.txt
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\Lidar\ExportData"
set DATA[0].ArchivesPath="C:\AustralData\instruments\bolivie\private\measurement"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=20*
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=0
set DATA[0].YearLen=4
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

set DATA[1].SendMethod=sync
set DATA[1].DataPath="C:\AustralData\instruments\bolivie\private\config\lidar"
set DATA[1].ArchivesPath=
set DATA[1].RemotePath="/home/config/"
set DATA[1].ToRename=
set DATA[1].Mask=*
set DATA[1].ArchiveFormat=
set DATA[1].ArchiveCurrDay=
set DATA[1].YearPos=
set DATA[1].YearLen=
set DATA[1].MonthPos=
set DATA[1].MonthLen=
set DATA[1].DayPos=
set DATA[1].DayLen=
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=
