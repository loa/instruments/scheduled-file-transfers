:: CODENAME
set CODENAME=Granulometer AIRMODUS

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=0

:: SCHEDULED TASK
set START_TIME=07
set EVERY_MIN=15

:: FILES
:: 20240618_131852_3152304123_CPC (ID 1).par
:: 20240618_131852_3152304123_CPC (ID 1).dat
:: 20240618_131852_9012401_PSM 2_dNdlogDp.csv
:: 20240618_131852_9012401_PSM 2.0 (ID 0).par
:: 20240618_131852_9012401_PSM 2.0 (ID 0).dat
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\AIRmodus\data"
set DATA[0].ArchivesPath="C:\AIRmodus\data\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=20*
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=0
set DATA[0].YearPos=0
set DATA[0].YearLen=4
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

