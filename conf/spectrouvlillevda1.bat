:: CODENAME
set CODENAME=SpectroUV VDA1

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30

:: SEND
set METHOD=put
set START_TIME=09
set EVERY_MIN=15

:: FILES
:: GLOBAL_20240101_0359_11500
set DATA[0].SendMethod=put
set DATA[0].DataPath="D:\Bentham\Spectra"
set DATA[0].ArchivesPath="D:\Bentham\Spectra\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=GLOBAL_*.dat
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=7
set DATA[0].YearLen=4
set DATA[0].MonthPos=11
set DATA[0].MonthLen=2
set DATA[0].DayPos=13
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

:: DIFFUSE_20240101_0414_5793
set DATA[1].SendMethod=put
set DATA[1].DataPath="D:\Bentham\Spectra"
set DATA[1].ArchivesPath="D:\Bentham\Spectra\LOA"
set DATA[1].RemotePath="/home/"
set DATA[1].ToRename=
set DATA[1].Mask=DIFFUSE_*.dat
set DATA[1].ArchiveFormat=YYYYMMDD
set DATA[1].ArchiveCurrDay=1
set DATA[1].YearPos=8
set DATA[1].YearLen=4
set DATA[1].MonthPos=12
set DATA[1].MonthLen=2
set DATA[1].DayPos=14
set DATA[1].DayLen=2
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=