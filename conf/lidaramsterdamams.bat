:: CODENAME
set CODENAME=Lidar AMS

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30000

:: SCHEDULED TASK
set START_TIME=02
set EVERY_MIN=10

:: FILES
:: l2410409.094567
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\Lidar\ExportData"
set DATA[0].ArchivesPath="C:\AustralData\instruments\ams\private\measurement"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=20*
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=0
set DATA[0].YearLen=4
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=9
set DATA[0].HourLen=2
set DATA[0].MinPos=11
set DATA[0].MinLen=2
set DATA[0].SecPos=
set DATA[0].SecLen=

rem set DATA[1].SendMethod=sync
rem set DATA[1].DataPath=""
rem set DATA[1].ArchivesPath=
rem set DATA[1].RemotePath="/home/calib/"
rem set DATA[1].ToRename=
rem set DATA[1].Mask=
rem set DATA[1].ArchiveFormat=
rem set DATA[1].ArchiveCurrDay=
rem set DATA[1].YearPos=
rem set DATA[1].YearLen=
rem set DATA[1].MonthPos=
rem set DATA[1].MonthLen=
rem set DATA[1].DayPos=
rem set DATA[1].DayLen=
rem set DATA[1].HourPos=
rem set DATA[1].HourLen=
rem set DATA[1].MinPos=
rem set DATA[1].MinLen=
rem set DATA[1].SecPos=
rem set DATA[1].SecLen=

set DATA[1].SendMethod=sync
set DATA[1].DataPath="C:\AustralData\instruments\ams\private\config\lidar"
set DATA[1].ArchivesPath=
set DATA[1].RemotePath="/home/config/"
set DATA[1].ToRename=
set DATA[1].Mask=*
set DATA[1].ArchiveFormat=
set DATA[1].ArchiveCurrDay=
set DATA[1].YearPos=
set DATA[1].YearLen=
set DATA[1].MonthPos=
set DATA[1].MonthLen=
set DATA[1].DayPos=
set DATA[1].DayLen=
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=
