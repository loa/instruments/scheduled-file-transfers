:: CODENAME
set CODENAME=Nephelometer 4000

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30

:: SCHEDULED TASK
set START_TIME=07
set EVERY_MIN=15

:: FILES
:: nephelometerV4000_PM10.txt
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\NephelometerV4000"
set DATA[0].ArchivesPath="C:\NephelometerV4000\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=nephelometerV4000_PM10.txt
set DATA[0].Mask=20*.txt
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=0
set DATA[0].YearLen=4
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
