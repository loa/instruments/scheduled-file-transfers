:: CODENAME
set CODENAME=Fluxmeter Kipp Zonen

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=0

:: SCHEDULED TASK
set START_TIME=04
set EVERY_MIN=15

:: FILES
:: Flux_LOA_2022_03_11_2301.dat
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\Campbellsci\LoggerNet\DATA"
set DATA[0].ArchivesPath="C:\Campbellsci\LoggerNet\DATA\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=Flux_LOA_*.dat
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=9
set DATA[0].YearLen=4
set DATA[0].MonthPos=14
set DATA[0].MonthLen=2
set DATA[0].DayPos=17
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

:: Flux_EtalLOA_2020_08_31_0001.dat
set DATA[1].SendMethod=put
set DATA[1].DataPath="C:\Campbellsci\LoggerNet\DATA"
set DATA[1].ArchivesPath="C:\Campbellsci\LoggerNet\DATA\LOA"
set DATA[0].RemotePath="/home/"
set DATA[1].ToRename=
set DATA[1].Mask=Flux_EtalLOA_*.dat
set DATA[1].ArchiveFormat=YYYYMMDD
set DATA[1].ArchiveCurrDay=1
set DATA[1].YearPos=13
set DATA[1].YearLen=4
set DATA[1].MonthPos=18
set DATA[1].MonthLen=2
set DATA[1].DayPos=21
set DATA[1].DayLen=2
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=
