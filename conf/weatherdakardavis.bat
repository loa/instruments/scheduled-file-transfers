:: CODENAME
set CODENAME=Weather DAVIS

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=2

:: SCHEDULED TASK
set START_TIME=10
set EVERY_MIN=15

:: FILES
:: download.txt
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\WeatherLink\MBOUR"
set DATA[0].RemotePath="/home/"
set DATA[0].ArchivesPath=
set DATA[0].ToRename=
set DATA[0].Mask=download.txt
set DATA[0].ArchiveFormat=
set DATA[0].ArchiveCurrMonth=
set DATA[0].ArchiveCurrDay=
set DATA[0].YearPos=
set DATA[0].YearLen=
set DATA[0].MonthPos=
set DATA[0].MonthLen=
set DATA[0].DayPos=
set DATA[0].DayLen=
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

:: 2022-06.wlk
set DATA[1].SendMethod=put
set DATA[1].DataPath="C:\WeatherLink\MBOUR"
set DATA[1].RemotePath="/home/"
set DATA[1].ArchivesPath=
set DATA[1].ToRename=
set DATA[1].Mask=*.wlk
set DATA[1].ArchiveFormat=
set DATA[1].ArchiveCurrMonth=
set DATA[1].ArchiveCurrDay=
set DATA[1].YearPos=0
set DATA[1].YearLen=4
set DATA[1].MonthPos=5
set DATA[1].MonthLen=2
set DATA[1].DayPos=
set DATA[1].DayLen=
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=
