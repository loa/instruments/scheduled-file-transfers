:: CODENAME
set CODENAME=Lidar LIFE

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30000

:: SCHEDULED TASK
set START_TIME=02
set EVERY_MIN=10

:: FILES
:: LI2510920.415857
set DATA[0].SendMethod=put
set DATA[0].DataPath="D:\LIFE\RawData"
set DATA[0].ArchivesPath="D:\LIFE\AustralData\instruments\life\private\measurement"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=LI*
set DATA[0].ArchiveFormat=YYMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=2
set DATA[0].YearLen=2
set DATA[0].MonthPos=4
set DATA[0].MonthLen=1
set DATA[0].DayPos=5
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=

set DATA[1].SendMethod=sync
set DATA[1].DataPath="D:\LIFE\AustralData\instruments\life\private\calibration"
set DATA[1].ArchivesPath=
set DATA[1].RemotePath="/home/calib/"
set DATA[1].ToRename=
set DATA[1].Mask=l*
set DATA[1].ArchiveFormat=
set DATA[1].ArchiveCurrDay=
set DATA[1].YearPos=
set DATA[1].YearLen=
set DATA[1].MonthPos=
set DATA[1].MonthLen=
set DATA[1].DayPos=
set DATA[1].DayLen=
set DATA[1].HourPos=
set DATA[1].HourLen=
set DATA[1].MinPos=
set DATA[1].MinLen=
set DATA[1].SecPos=
set DATA[1].SecLen=

set DATA[2].SendMethod=sync
set DATA[2].DataPath="D:\LIFE\AustralData\instruments\life\private\config\lidar"
set DATA[2].ArchivesPath=
set DATA[2].RemotePath="/home/config/"
set DATA[2].ToRename=
set DATA[2].Mask=*
set DATA[2].ArchiveFormat=
set DATA[2].ArchiveCurrDay=
set DATA[2].YearPos=
set DATA[2].YearLen=
set DATA[2].MonthPos=
set DATA[2].MonthLen=
set DATA[2].DayPos=
set DATA[2].DayLen=
set DATA[2].HourPos=
set DATA[2].HourLen=
set DATA[2].MinPos=
set DATA[2].MinLen=
set DATA[2].SecPos=
set DATA[2].SecLen=
