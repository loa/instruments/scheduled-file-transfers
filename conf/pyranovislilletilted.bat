:: CODENAME
set CODENAME=Pyrano TILTED

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30

:: SEND
set METHOD=put
set START_TIME=06
set EVERY_MIN=15

:: FILES
:: 20201021_1534.pyr
set DATA[0].SendMethod=put
set DATA[0].DataPath="D:\LSI-Lastem\DATA"
set DATA[0].ArchivesPath="D:\LSI-Lastem\DATA\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=20*.pyr
set DATA[0].ArchiveFormat=YYYYMMDD
set DATA[0].ArchiveCurrDay=1
set DATA[0].YearPos=0
set DATA[0].YearLen=4
set DATA[0].MonthPos=4
set DATA[0].MonthLen=2
set DATA[0].DayPos=6
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
