:: CODENAME
set CODENAME=CPC 3750

:: FTP
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%ARG%
set FTP_MAX=30

:: SCHEDULED TASK
set START_TIME=06
set EVERY_MIN=15

:: FILES
:: CPC_375010223601_20250218.csv
set DATA[0].SendMethod=put
set DATA[0].DataPath="C:\Users\LOA\TSI\Aerosol Instrument Manager CPC\AutoExport"
set DATA[0].ArchivesPath="C:\Users\LOA\TSI\Aerosol Instrument Manager CPC\AutoExport\LOA"
set DATA[0].RemotePath="/home/"
set DATA[0].ToRename=
set DATA[0].Mask=CPC_375010223601_*.csv
set DATA[0].ArchiveFormat=YYYYMM
set DATA[0].ArchiveCurrDay=0
set DATA[0].YearPos=17
set DATA[0].YearLen=4
set DATA[0].MonthPos=21
set DATA[0].MonthLen=2
set DATA[0].DayPos=23
set DATA[0].DayLen=2
set DATA[0].HourPos=
set DATA[0].HourLen=
set DATA[0].MinPos=
set DATA[0].MinLen=
set DATA[0].SecPos=
set DATA[0].SecLen=
