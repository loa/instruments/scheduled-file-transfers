# Scheduled File Transfers

## Known Issues
### Access denied
On Windows 10, when you execute `deploy_uploader.bat` for the first time, you might obtain several lines *Access denied* in the prompt.<br />
It happens when the `certutil` command is executed for downloading WinSCP primary binaries on GitLab.

To fix it, (to complete)