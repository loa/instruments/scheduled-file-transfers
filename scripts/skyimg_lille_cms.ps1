# ------------------------------------------------------------------------
# NAME     : skyimg_lille_cms.ps1
# AUTHOR   : Romain De Filippi, LOA
# EMAIL    : romain.defilippi@univ-lille.fr
# DATE     : 2021-06-03
#
# COMMENTS : This script sends images and evaluations data (FindClouds)
# to LOA data storage. It uses a timestamp file for each execution
# and sends only new files.
# ------------------------------------------------------------------------

# Variables
$pathLOAFolder = "C:\LOA"
$pathDataFolder = "D:\ProgramData\cloudcam\ccam_01065"

# ENVIRONMENT - PLEASE, DO NOT MODIFY THIS PART -
$nameScript = "skyimg_lille_cms"
$login = "skyimglillecms"
$pathLogsFolder = "$pathLOAFolder\logs\$nameScript"
$pathConfFolder = "$pathLOAFolder\conf"
$fileLastUpload = "$pathLogsFolder\last_upload.log"


# PROGRAM - PLEASE, DO NOT MODIFY THIS PART -

# Get the run timestamp (for next upload)
$logTime = Get-Date -Format "MM-dd-yyyy HH:mm:ss"

# Test : if no logs folder, create it
if (!(Test-Path -Path $pathLogsFolder)) { New-Item -ItemType directory -Path $pathLogsFolder }

# Test : if last_upload.log exists, get content ELSE use the first UNIX date for all datas
if (Test-Path $fileLastUpload -PathType leaf) { $dateLastUpload = Get-Content -Path $fileLastUpload } else { $dateLastUpload = "01-01-1970 00:00:00" }

# Load the Assembly and setup the session properties
try
{
    # Load WinSCP .NET Assembly
    Add-Type -Path "$pathLOAFolder\bin\winscp\WinSCPnet.dll"
    
    # Setup session options
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Ftp
		FtpSecure = [WinSCP.FtpSecure]::Explicit
        HostName = "loa-collect.univ-lille.fr"
        UserName = $login
        Password = Get-Content -Path "$pathConfFolder\$login.key" -ErrorAction Stop
        GiveUpSecurityAndAcceptAnyTlsHostCertificate = 1
    }

    $session = New-Object WinSCP.Session

    # Gather all files with '20' (two first numbers of year) : .jpg, .png, cloudiness.txt
    $fileList = Get-ChildItem -Path $pathDataFolder -Filter *20* -Recurse -File | 
			Where-Object { $_.LastWriteTime -ge $dateLastUpload }

    # Connect and send files, then close session
    try
    {
        # Connect
        # WARNING : Debug log file could be too big if activated permanently
        #$session.DebugLogPath = "$pathLogsFolder\$nameScript.log"
		$session.Open($sessionOptions)

        $transferOptions = New-Object WinSCP.transferOptions
        $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary

        # Send
        foreach ($file in $fileList)
        {
            $transferResult = $session.PutFiles($file.FullName, "/home/", $False, $transferOptions)
            foreach ($transfer in $transferResult.Transfers)
            {
                Write-Host "Upload of $($transfer.FileName) succeeded"
            }
        }
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }
	
	# Update the last upload timestamp
	$logTime | Out-File -FilePath $fileLastUpload
    exit 0
}

# Catch any errors
catch
{
    Write-Host "Error: $($_.Exception.Message)"
    exit 1
}
