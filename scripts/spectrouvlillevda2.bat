@echo off
rem # ------------------------------------------------------------------------
rem # NAME     : spectrouvlillevda2.sh
rem # AUTHOR   : Romain De Filippi, LOA
rem # EMAIL    : romain.defilippi@univ-lille.fr
rem # DATE     : 2022-01-26
rem #
rem # COMMENTS : This script sends *.dat files to LOA FTP server and moves
rem #            them into LOA folder.
rem #            Please, do not modify this script.
rem # ------------------------------------------------------------------------

rem - ENVIRONMENT -
setlocal EnableDelayedExpansion
title Deploy Upload Environment - LOA
rem - /ENVIRONMENT -


rem - VARIABLES -
set NAME_SCRIPT=%~n0
set DEBUG=0
set CMD_LOG=
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%NAME_SCRIPT%
set FTP_MAX=30
set PATH_LOA=C:\LOA
set PATH_BIN=%PATH_LOA%\bin
set PATH_WINSCP=%PATH_BIN%\winscp
set PATH_CONF=%PATH_LOA%\conf
set PATH_TMP=%PATH_LOA%\tmp
set PATH_LOGS=%PATH_LOA%\logs

set PATH_DATA=D:\Bentham\Spectra
set PATH_ARCH=%PATH_DATA%\LOA
set PATH_BEN=%PATH_DATA%\BEN
rem - /VARIABLES -


rem - PROGRAM -
rem Test : if the task doesn't already exist, create it
schtasks /query /TN "%NAME_SCRIPT%" >NUL 2>&1
if %errorlevel% NEQ 0 schtasks /create /sc minute /mo 15 /tn "\%NAME_SCRIPT%" /tr "%PATH_BIN%\%NAME_SCRIPT%" /st 00:10:00

rem Test : if a key file not exists, create it
if not exist %PATH_CONF%\%FTP_USER%.key (
  echo WARNING : no key file found ^^!
  goto password
) else (
  goto transfer
)

:password
echo.
set /p password=FTP password (%FTP_USER%) ? 
if /i "%password%" == "" goto password
echo %password% > %PATH_CONF%\%FTP_USER%.key

:transfer
rem Get the password for FTP account
set /p PASS_KEY=<%PATH_CONF%\%FTP_USER%.key
rem Remove blank space at the end of line (if exists)
set FTP_PASS=%PASS_KEY: =%

rem List of files to send 
dir /b /a-d %PATH_DATA%\*.dat > %PATH_TMP%\%NAME_SCRIPT%.tmp

rem Filtered list of files to send (FTP_MAX limit)
copy nul %PATH_TMP%\%NAME_SCRIPT%.txt
for /f "Tokens=1,* Delims=:" %%i in ('findstr /n /r . %PATH_TMP%\%NAME_SCRIPT%.tmp') do if %%i LEQ %FTP_MAX% echo %%j >> %PATH_TMP%\%NAME_SCRIPT%.txt

rem Generate a WinSCP script file
(
  echo option batch abort
  echo option confirm off
  echo open ftpes://%FTP_USER%:%FTP_PASS%@%FTP_HOST% -certificate=*
  echo option transfer binary
  rem Generate "put" command for each line in list file
  for /f %%i in (%PATH_TMP%\%NAME_SCRIPT%.txt) do echo put "%PATH_DATA%\%%i" /home/
  echo close
  echo exit
) > %PATH_TMP%\%NAME_SCRIPT%.wscript

rem Delete the temporary file
del %PATH_TMP%\%NAME_SCRIPT%.tmp

rem Define options for WinSCP verbose logs (DEBUG MODE) 
if "%DEBUG%" == "1" ( set CMD_LOG=/log=%PATH_LOGS%\%NAME_SCRIPT%.winscp.log /loglevel=-1 /logsize=5*1M )

rem Execute WinSCP with the generated script
%PATH_WINSCP%\WinSCP.com %CMD_LOG% /script=%PATH_TMP%\%NAME_SCRIPT%.wscript

rem Test : if WINSCP fails, exit with error
if %ERRORLEVEL% NEQ 0 goto error

rem Move .ben files to BEN folder
if not exist %PATH_BEN% md %PATH_BEN%
move %PATH_DATA%\*.ben %PATH_BEN%> NUL

rem Test : if PATH_ARCH not exist, create it
if not exist %PATH_ARCH% md %PATH_ARCH%

rem Read the list of sent files
for /f %%i in ('type %PATH_TMP%\%NAME_SCRIPT%.txt') do (
  set LINE=%%i
  set YYYY=!LINE:~0,4!
  set MM=!LINE:~4,2!
  set DD=!LINE:~6,2!
  
  rem Create directories YYYY/MM/DD
  if not exist %PATH_ARCH%\!YYYY! md %PATH_ARCH%\!YYYY!
  if not exist %PATH_ARCH%\!YYYY!\!MM! md %PATH_ARCH%\!YYYY!\!MM!
  if not exist %PATH_ARCH%\!YYYY!\!MM!\!DD! md %PATH_ARCH%\!YYYY!\!MM!\!DD!
  
  rem Move files into the archive folder
  move %PATH_DATA%\%%i %PATH_ARCH%\!YYYY!\!MM!\!DD!
)

rem Delete the temporary file
del %PATH_TMP%\%NAME_SCRIPT%.txt

:quit
echo.
echo SUCCESS : FTP transfer complete ^^!
timeout /t 5 /nobreak
exit /b 0

:error
echo.
echo ERROR : FTP transfer failed ^^!
timeout /t 5 /nobreak
exit /b 1

rem - /PROGRAM -