@echo off
rem # ------------------------------------------------------------------------
rem # NAME     : weatherlilledavis.bat
rem # AUTHOR   : Romain De Filippi, LOA
rem # EMAIL    : romain.defilippi@univ-lille.fr
rem # DATE     : 2021-06-18
rem #
rem # COMMENTS : This script sends 48 hours ASCII data and archives in .wlk
rem #            to LOA data storage. It uses a timestamp file for each execution
rem #            and sends only new files.
rem # ------------------------------------------------------------------------

rem - ENVIRONMENT -
setlocal EnableDelayedExpansion
title Deploy Upload Environment - LOA
rem - /ENVIRONMENT -


rem - VARIABLES -
set NAME_SCRIPT=%~n0
set DEBUG=0
set CMD_LOG=
set FTP_HOST=ftp.loa.univ-lille.fr
set FTP_USER=%NAME_SCRIPT%
set FTP_MAX=30
set PATH_LOA=C:\LOA
set PATH_BIN=%PATH_LOA%\bin
set PATH_WINSCP=%PATH_BIN%\winscp
set PATH_CONF=%PATH_LOA%\conf
set PATH_TMP=%PATH_LOA%\tmp
set PATH_LOGS=%PATH_LOA%\logs

set PATH_DATA=C:\WeatherLink\MBOUR
rem - /VARIABLES -

rem - PROGRAM -
rem Test : if the task doesn't already exist, create it
schtasks /query /TN "%NAME_SCRIPT%" >NUL 2>&1
if %errorlevel% NEQ 0 schtasks /create /sc minute /mo 15 /tn "\%NAME_SCRIPT%" /tr "%PATH_BIN%\%NAME_SCRIPT%" /st 00:10:00

rem Test : if a key file not exists, create it
if not exist %PATH_CONF%\%FTP_USER%.key (
  echo WARNING : no key file found ^^!
  goto password
) else (
  goto transfer
)

:password
echo.
set /p password=FTP password (%FTP_USER%) ? 
if /i "%password%" == "" goto password
echo %password% > %PATH_CONF%\%FTP_USER%.key

:transfer
rem Get the password for FTP account
set /p PASS_KEY=<%PATH_CONF%\%FTP_USER%.key
rem Remove blank space at the end of line (if exists)
set FTP_PASS=%PASS_KEY: =%

rem Get the previous month (YYYY-MM) for .wlk
for /f %%A in ('wmic os get localDateTime') do for %%B in (%%A) do set ts=%%B
set /a "YYYY=%ts:~,4%, MM=1%ts:~4,2%"
if %MM% Equ 101 (set /a "YYYY-=1, MM=112") else set /a "MM-=1"
set WLK_PREV=%YYYY%-%MM:~1%.wlk
set WLK_CURR=%DATE:~6,4%-%DATE:~3,2%.wlk

rem Generate a WinSCP script file
(
  echo option batch abort
  echo option confirm off
  echo open ftpes://%FTP_USER%:%FTP_PASS%@%FTP_HOST% -certificate=*
  echo option transfer binary
  echo put %PATH_DATA%\download.txt /home/
  echo put %PATH_DATA%\%WLK_CURR% /home/
  echo put %PATH_DATA%\%WLK_PREV% /home/
  echo close
  echo exit
) > %PATH_TMP%\%NAME_SCRIPT%.wscript

rem Define options for WinSCP verbose logs (DEBUG MODE) 
if "%DEBUG%" == "1" ( set CMD_LOG=/log=%PATH_LOGS%\%NAME_SCRIPT%.winscp.log /loglevel=-1 /logsize=5*1M )

rem Execute WinSCP with the generated script
%PATH_WINSCP%\WinSCP.com %CMD_LOG% /script=%PATH_TMP%\%NAME_SCRIPT%.wscript

rem Test : if WINSCP fails, exit with error
if %ERRORLEVEL% NEQ 0 goto error

:quit
echo.
echo SUCCESS : FTP transfer complete ^^!
timeout /t 5 /nobreak
exit /b 0

:error
echo.
echo ERROR : FTP transfer failed ^^!
timeout /t 5 /nobreak
exit /b 1

rem - /PROGRAM -