#!/bin/bash
###################################################################
# Script Name   : aethalolilleae33.sh                             #
# Description   : Upload data aethalometer AE33 to loa-collect    #
# Args          : -                                               #
# Author        : Romain De Filippi                               #
# Email         : romain.defilippi@univ-lille.fr                  #
###################################################################

# VARIABLES
# Environment
DEBUG=0
LOCK_FILE="/tmp/aethalolilleae33.lock"
LIST_FILES="/home/chimie/list_files.txt"

# FTP settings
FTP_HOST="loa-collect.univ-lille.fr"
# Note : FTP credentials are stored in ~/.netrc

# Folders path
PATH_DATA="/home/chimie/DATABANK/AE33"
PATH_ARCH="/home/chimie/DATABANK/LOA/AE33"

# PROGRAM
# Test if a process is running
if [[ -f "${LOCK_FILE}" ]]; then
  if [[ "${DEBUG}" == "1" ]]; then
    echo "Lock file present : ${LOCK_FILE}"
    echo "Aborting ..."
  fi
  exit 0
else
  touch "${LOCK_FILE}"

  # Create folders
  if [[ ! -d "${PATH_ARCH}" ]]; then
    if [[ "${DEBUG}" == "1" ]]; then
      echo "mkdir -p ${PATH_ARCH}"
    fi
    mkdir -p "${PATH_ARCH}"
  fi

  # List of files to send
  if [[ "${DEBUG}" == "1" ]]; then
    echo "find ${PATH_DATA} -type f"
  fi
  find "${PATH_DATA}" -type f -exec echo {} >> "${LIST_FILES}" \;

  while read line; do
    filename=`basename "${line}"`
    extension="${filename: -3}"

    # Filename example : LILLE_AE33_AE33-S02-00267_20210312140008
    year="${filename:26:4}"
    month="${filename:30:2}"
    day="${filename:32:2}"

    # Create the folder structure before moving file
    if [[ ! -d "${PATH_ARCH}/${year}/${month}/${day}" ]]; then
      if [[ "${DEBUG}" == "1" ]]; then
        echo "mkdir -p ${PATH_ARCH}/${year}/${month}/${day}"
      fi
      mkdir -p "${PATH_ARCH}/${year}/${month}/${day}"
    fi

    # If the filename is not "log", send it
    if [[ "${extension}" != "log" ]] ; then
      # LFTP command
      lftp -e "set ssl:verify-certificate false; set net:max-retries 3; set net:timeout 1m; put -O /home/ ${line}; bye" ${FTP_HOST}

      # LFTP status code (0 -> if last put success / 1 -> if last put failed)
      status_code=$?

      # If FTP success, move files in PATH_ARCH
      if [[ "${status_code}" == "0" ]]; then
        # Move the file
        if [[ "${DEBUG}" == "1" ]]; then
          echo "mv ${line} ${PATH_ARCH}/${year}/${month}/${day}/"
        fi
        mv "${line}" "${PATH_ARCH}/${year}/${month}/${day}/"
      fi
    fi

    # If the filename is "log", move only if not the current day
    if [[ "${extension}" == "log" ]] ; then
      YYYYMMDD=$(date '+%Y%m%d')
      if [[ "${year}${month}${day}" != "${YYYYMMDD}" ]]; then
        # Move the file
        if [[ "${DEBUG}" == "1" ]]; then
          echo "mv ${line} ${PATH_ARCH}/${year}/${month}/${day}/"
        fi
        mv "${line}" "${PATH_ARCH}/${year}/${month}/${day}/"
      fi
    fi

  done <"${LIST_FILES}"

  # Delete empty old directories in PATH_DATA
  find "${PATH_DATA}" -empty -type d -mtime +5 -exec rm -rf {} \;

  # Delete list of sent files
  rm "${LIST_FILES}"

  # Delete the lock file
  rm "${LOCK_FILE}"
fi

exit 0