#!/bin/bash
###################################################################
# Script Name   : skyimglilleeko.sh                               #
# Description   : Upload data skyimager EKO to loa-collect        #
# Args          : -                                               #
# Author        : Romain De Filippi                               #
# Email         : romain.defilippi@univ-lille.fr                  #
###################################################################

# VARIABLES
# Environment
DEBUG=0
LOCK_FILE="/tmp/skyimglilleeko.lock"
LIST_FILES="/home/pi/list_files.txt"

# FTP settings
FTP_HOST="ftp.loa.univ-lille.fr"
# Note : FTP credentials are stored in ~/.netrc

# Folders path
PATH_DATA="/home/pi/Desktop/DATA_SKYIMAGER/asi_16217"
PATH_ARCH="/home/pi/Desktop/DATA_SKYIMAGER/LOA"

# PROGRAM
# Test if a process is running
if [[ -f "${LOCK_FILE}" ]]; then
  if [[ "${DEBUG}" == "1" ]]; then
    echo "Lock file present : ${LOCK_FILE}" 
    echo "Aborting ..."
  fi
  exit 0
else
  touch "${LOCK_FILE}"

  # Create folders
  if [[ ! -d "${PATH_ARCH}" ]]; then
    if [[ "${DEBUG}" == "1" ]]; then
      echo "mkdir -p ${PATH_ARCH}"
    fi 
    mkdir -p "${PATH_ARCH}"
  fi

  # List of files to send
  if [[ "${DEBUG}" == "1" ]]; then
    echo "find ${PATH_DATA} -type f"
  fi 
  find "${PATH_DATA}" -type f -exec echo {} > "${LIST_FILES}" \;

  while read line; do
    # LFTP command
    lftp -e "set ssl:verify-certificate false; set net:max-retries 3; set net:timeout 1m; put -O /home/ ${line}; bye" ${FTP_HOST}

    # LFTP status code (0 -> if last put success / 1 -> if last put failed)
    status_code=$?

    # If FTP success, move files in PATH_ARCH
    if [[ "${status_code}" == "0" ]]; then
      filename=`basename "${line}"`
      extension="${filename: -3}"

      # If the filename doesn't finish by "jpg" or "csv", skip it
      if [[ "${extension}" != "jpg" ]] && [[ "${extension}" != "csv" ]]; then
        if [[ "${DEBUG}" == "1" ]]; then
          echo "Warning : '${filename}' is not an expected file extension ! Skipping ..."
        fi
        continue
      fi

      # Read date when .jpg file
      if [[ "${extension}" == "jpg" ]]; then
        year="${filename:0:4}"
        month="${filename:4:2}"
        day="${filename:6:2}"
      fi

      # Read date when .csv file
      if [[ "${extension}" == "csv" ]]; then
        year="${filename:15:4}"
        month="${filename:19:2}"
        day="${filename:21:2}"
      fi

      # Create the folder structure before moving file
      if [[ ! -d "${PATH_ARCH}/${year}/${month}/${day}" ]]; then
        if [[ "${DEBUG}" == "1" ]]; then
          echo "mkdir -p ${PATH_ARCH}/${year}/${month}/${day}"
        fi
        mkdir -p "${PATH_ARCH}/${year}/${month}/${day}"
      fi

      # Move the file
      if [[ "${DEBUG}" == "1" ]]; then
        echo "mv ${line} ${PATH_ARCH}/${year}/${month}/${day}/" 
      fi
      mv "${line}" "${PATH_ARCH}/${year}/${month}/${day}/" 
    fi
  done <"${LIST_FILES}"

  # Delete empty old directories in PATH_DATA
  find "${PATH_DATA}" -empty -type d -mtime +5 -exec rm -rf {} \;
  
  # Delete old datas in PATH_ARCH
  find "${PATH_ARCH}" -type d -mtime +30 -exec rm -rf {} \;
  
  # Delete list of sent files
  rm "${LIST_FILES}"

  # Delete the lock file
  rm "${LOCK_FILE}"
fi

exit 0
