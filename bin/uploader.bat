:: ------------------------------------------------------------------------
:: NAME     : uploader.bat
:: AUTHOR   : Romain De Filippi - LOA
:: EMAIL    : romain.defilippi@univ-lille.fr
:: DATE     : 2024-03-14
::
:: COMMENTS : This script creates scheduled tasks (when no argument) and
::            read the defined conf file and send data to FTP LOA server.
::            PLEASE, DO NOT EDIT THIS SCRIPT !
:: ------------------------------------------------------------------------

rem Environment
@echo off
setlocal EnableDelayedExpansion
cls

rem Variables
set ARG=%~1
set CURR_DATETIME=%DATE:~6,4%-%DATE:~3,2%-%DATE:~0,2% %TIME:~0,8%
set CONF_FILE=%ARG%.bat
set LIBRARY=_library.bat
set DEBUG=0

rem Paths
set PATH_LOA=C:\LOA
set PATH_AUTH=%PATH_LOA%\auth
set PATH_BIN=%PATH_LOA%\bin
set PATH_CONF=%PATH_LOA%\conf
set PATH_LOGS=%PATH_LOA%\logs
set PATH_TMP=%PATH_LOA%\tmp

rem GitLab repository
set URL_REPO=https://gitlab.univ-lille.fr/loa/instruments/scheduled-file-transfers/-/raw/master

rem Remove all temporary files
del %PATH_TMP%\%ARG%.* >NUL 2>&1

rem Test the argument
if "%ARG%" == "" goto instrument
if "%ARG%" == "update" goto update

:conf
rem Check if the configuration file of the specified instrument exists
if not exist %PATH_CONF%\%CONF_FILE% (
	echo.
	echo [ERROR] File %PATH_CONF%\%CONF_FILE% not found ^^!
	echo Execute %~0 with no argument for downloading a configuration file.
	goto error
)

rem Read the configuration file
echo Reading config file : %PATH_CONF%\%CONF_FILE%
call %PATH_CONF%\%CONF_FILE%
echo Done ^^!

:schedule
rem Check if the instrument upload is scheduled in the system 
schtasks /query /TN "%ARG%" >NUL 2>&1
if %ERRORLEVEL% NEQ 0 (
  if %DEBUG% == 1 echo [DEBUG] schtasks /create /sc minute /mo %EVERY_MIN% /tn "\%ARG%" /tr "%PATH_BIN%\uploader.bat %ARG%" /st 00:%START_TIME%
  schtasks /create /sc minute /mo %EVERY_MIN% /tn "\%ARG%" /tr "%PATH_BIN%\uploader.bat %ARG%" /st 00:%START_TIME% >NUL 2>&1
)

rem Test : if a key file not exists, create it
if not exist %PATH_AUTH%\%FTP_USER%.key (
  echo [WARNING] No key file found ^^!
  goto password
) else (
  goto read_password
)

:read_password
rem Get the password for FTP account
set /p PASS_KEY=<%PATH_AUTH%\%FTP_USER%.key
rem Remove blank space at the end of line (if exists)
set FTP_PASS=%PASS_KEY: =%

rem Update the window title
title Upload %CODENAME% data - LOA

set nb_data=0
:define_data
rem Define the number of data sources (declared in config file)
if defined DATA[%nb_data%].Mask ( 
  set /a nb_data+=1
  goto define_data
)
rem Adjust the value for the next for loop
set /a nb_data-=1

:rename
rem Rename generic files (see DATA[x].ToRename value)
for /l %%f in (0,1,%nb_data%) do (
  if not "!DATA[%%f].ToRename!"=="" (
    for /f %%a in ('dir /b /a-d !DATA[%%f].DataPath!\!DATA[%%f].ToRename!') do (
	  rem Add a zero for hours under 10
      set timezero=%time: =0%
	  if %DEBUG% == 1 echo [DEBUG] move !DATA[%%f].DataPath!\%%a !DATA[%%f].DataPath!\!DATE:~6,4!!DATE:~3,2!!DATE:~0,2!_!timezero:~0,2!!timezero:~3,2!!timezero:~6,2!_%%a
      move !DATA[%%f].DataPath!\%%a !DATA[%%f].DataPath!\!DATE:~6,4!!DATE:~3,2!!DATE:~0,2!_!timezero:~0,2!!timezero:~3,2!!timezero:~6,2!_%%a >NUL 2>&1
	  if %ERRORLEVEL% NEQ 0 (
	    echo [ERROR] Unable to rename !DATA[%%f].DataPath!\%%a
	    goto error
	  )
	)
  )
)

rem Upload data (put / synchronize)
for /l %%f IN (0,1,%nb_data%) do (

	rem PUT
	if "!DATA[%%f].SendMethod!" == "put" (
		if %DEBUG% == 1 (
			echo.
			echo [DEBUG] call %PATH_BIN%\%LIBRARY% put !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].RemotePath!
		)
		call %PATH_BIN%\%LIBRARY% put !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].RemotePath!
	)

	rem SYNC
	if "!DATA[%%f].SendMethod!" == "sync" (
		if %DEBUG% == 1 (
			echo.
			echo [DEBUG] call %PATH_BIN%\%LIBRARY% sync !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].RemotePath!
		)
		call %PATH_BIN%\%LIBRARY% sync !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].RemotePath!
	)
	
	rem COMMON
	if !ERRORLEVEL! EQU 0 (
		if %DEBUG% == 1 (
			echo.
			echo [DEBUG] call %PATH_BIN%\%LIBRARY% common !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].SendMethod! %%f !DATA[%%f].RemotePath! !DATA[%%f].ArchivesPath!
		)
		call %PATH_BIN%\%LIBRARY% common !DATA[%%f].Mask! !DATA[%%f].DataPath! !DATA[%%f].SendMethod! %%f !DATA[%%f].RemotePath! !DATA[%%f].ArchivesPath!
	) else (
		if %DEBUG% == 1 echo [DEBUG] No common execution, ERRORLEVEL = !ERRORLEVEL!
	)

)
goto quit

:instrument
rem Get the INSTRUMENT ID
echo.
echo No instrument specified in argument ^^!
echo Would you like to download a configuration file for your instrument ? 
echo (note: you need to know the instrument ID to continue)

echo.
set /p script=Download the script [Y/n] ? 
if /i "%script%" == "" goto id
if /i "%script%" == "y" (
  :id
  echo.
  set /p instrument=INTRUMENT ID ^(in lowercase^) ? 
  if "!instrument!" == "" goto id
  
  rem Loop for downloading .bat and .ps1 files (then remove bad one)
  for %%e in (bat) do (
    call %PATH_BIN%\%LIBRARY% download !instrument!.%%e conf %PATH_LOA%
	if exist "%PATH_CONF%\!instrument!.%%e" goto execute
  )
  goto leaving
)
if /i "%script%" == "n" (
  :leaving
  echo.
  echo No script was downloaded or executed ^^!
  echo Exiting ...
  goto quit
)
exit /b

:execute
echo.
set /p launch=Execute the script [Y/n] ? 
if /i "%launch%" == "" goto uploader
if /i "%launch%" == "n" goto leaving
if /i "%launch%" == "Y" (
  :uploader
  if exist %PATH_CONF%\!instrument!.bat (
    call %PATH_BIN%\uploader.bat !instrument!
  )
  goto quit
)
goto execute

:update
rem Download the uploader script and WinSCP binaries
echo.
for %%b in (uploader.bat _library.bat WinSCP.com WinSCP.exe WinSCPnet.dll) do (
  echo Upgrading '%%b' from the repository ...
  if %DEBUG% == 1 echo call %PATH_BIN%\%LIBRARY% download %%b bin %PATH_LOA%
  call %PATH_BIN%\%LIBRARY% download %%b bin %PATH_LOA%
  if not exist "%PATH_BIN%\%%b" (
    echo Error : unable to find or download '%%b' from the repository ^^!
    goto error
  )
)
goto quit

:password
echo.
set /p password=FTP password (%FTP_USER%) ? 
if /i "%password%" == "" goto password
echo %password% > %PATH_AUTH%\%FTP_USER%.key
goto read_password

:error
timeout /t 5 /nobreak
exit /b 1

:quit
timeout /t 5 /nobreak
exit /b 0
