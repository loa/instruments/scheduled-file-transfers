:: ------------------------------------------------------------------------
:: NAME     : _library.bat
:: AUTHOR   : Romain De Filippi - LOA
:: EMAIL    : romain.defilippi@univ-lille.fr
:: DATE     : 2024-03-14
::
:: COMMENTS : Functions library used by uploader.bat
::            PLEASE, DO NOT EDIT THIS SCRIPT !
:: ------------------------------------------------------------------------

rem Environment
@echo off

rem  Variables
set ARG0=%~0
set ARG1=%~1
set ARG2=%~2
set ARG3=%~3
set ARG4=%~4
set ARG5=%~5
set ARG6=%~6
set ARG7=%~7

rem GitLab repository
set URL_REPO=https://gitlab.univ-lille.fr/loa/instruments/scheduled-file-transfers/-/raw/master

if "%ARG1%" NEQ "" call :%ARG1%
exit /b %ERRORLEVEL%

:list_files
rem Create a list of files to send (wildcard option)
copy nul %PATH_TMP%\%ARG%.tmp >NUL 2>&1
if %ERRORLEVEL% NEQ 0 echo [ERROR] Unable to create %PATH_TMP%\%ARG%.tmp file ^^!
if %DEBUG% == 1 echo [DEBUG] dir /b /a-d /o-n %ARG2%\%ARG3% ^> %PATH_TMP%\%ARG%.tmp
dir /b /a-d /o-n "%ARG2%\%ARG3%" > %PATH_TMP%\%ARG%.tmp
exit /b %ERRORLEVEL%

:filter_list
rem Create a  filtered list of files to send (FTP_MAX limit)
copy nul %PATH_TMP%\%ARG%.txt >NUL 2>&1
if %ERRORLEVEL% NEQ 0 echo [ERROR] Unable to create %PATH_TMP%\%ARG%.txt file ^^!
for /f "Tokens=1,* Delims=:" %%i in ('findstr /n /r . %PATH_TMP%\%ARG%.tmp') do (
  if %%i LEQ %FTP_MAX% (
    echo %%j >> %PATH_TMP%\%ARG%.txt
  )
)
exit /b %ERRORLEVEL%

:write_wscript
rem Write a WinSCP script which will be executed by the WinSCP binary
set PATH_DATA=!ARG2!
set METHOD=!ARG3!
set MASK_FILES=!ARG4!
set PATH_REMOTE=!ARG5!
set ID_DATA=!ARG6!
set /p LAST_UPLOAD=<%PATH_LOGS%\last_%ARG%_data%ID_DATA%.log
if "%LAST_UPLOAD%" == "" (
  set LAST_UPLOAD=1970-01-01 00:00:00
)

(
  echo option batch abort
  echo option confirm off
  echo open ftpes://_%FTP_USER%:%FTP_PASS%@%FTP_HOST% -certificate=*
  echo option transfer binary 
  if "%METHOD%" == "put" (
    for /f "delims=" %%i in (%PATH_TMP%\%ARG%.txt) do echo put "%PATH_DATA%\%%i" %PATH_REMOTE%
  )
  if "%METHOD%" == "sync" (
    rem echo synchronize remote -filemask="%MASK_FILES%>%LAST_UPLOAD%" "%PATH_DATA%" %PATH_REMOTE%
    echo synchronize remote -filemask="%MASK_FILES%>%LAST_UPLOAD%" "%PATH_DATA%" %PATH_REMOTE% -rawtransfersettings ExcludeEmptyDirectories=1
  )
  echo exit
  echo close
) > %PATH_TMP%\%ARG%.wscript
exit /b %ERRORLEVEL%

:winscp
rem Define options for WinSCP verbose logs (DEBUG MODE) 
if %DEBUG% == 1 set CMD_LOG=/log=%PATH_LOGS%\%ARG%.winscp.log /loglevel=-1 /logsize=5*1M

rem Execute WinSCP with the generated script
if %DEBUG% == 1 echo [DEBUG] %PATH_BIN%\WinSCP.com %CMD_LOG% /script=%PATH_TMP%\%ARG%.wscript
%PATH_BIN%\WinSCP.com %CMD_LOG% /script=%PATH_TMP%\%ARG%.wscript >NUL 2>&1
exit /b %ERRORLEVEL%

:archive
set ID_DATA=%ARG2%
set YearPos=!DATA[%ID_DATA%].YearPos!
set YearLen=!DATA[%ID_DATA%].YearLen!
set MonthPos=!DATA[%ID_DATA%].MonthPos!
set MonthLen=!DATA[%ID_DATA%].MonthLen!
set DayPos=!DATA[%ID_DATA%].DayPos!
set DayLen=!DATA[%ID_DATA%].DayLen!
set HourPos=!DATA[%ID_DATA%].HourPos!
set HourLen=!DATA[%ID_DATA%].HourLen!
set MinPos=!DATA[%ID_DATA%].MinPos!
set MinLen=!DATA[%ID_DATA%].MinLen!
set SecPos=!DATA[%ID_DATA%].SecPos!
set SecLen=!DATA[%ID_DATA%].SecLen!
set ArchiveFormat=!DATA[%ID_DATA%].ArchiveFormat!
set ArchivesPath=!DATA[%ID_DATA%].ArchivesPath!

if %DEBUG% == 1 echo [DEBUG] Today : %DATE:~6,4%%DATE:~3,2%%DATE:~0,2%

for /f "delims=" %%l in (%PATH_TMP%\%ARG%.txt) do (
  set filename=%%l 
  set year=!filename:~%YearPos%,%YearLen%!
  set month=!filename:~%MonthPos%,%MonthLen%!
  set day=!filename:~%DayPos%,%DayLen%!
  set hour=!filename:~%HourPos%,%HourLen%!
  set min=!filename:~%MinPos%,%MinLen%!
  set sec=!filename:~%SecPos%,%SecLen%!
  set move=1
  
  rem For instruments in YYMDD format, we rewrite date
  if "%ArchiveFormat%" == "YYMDD" (
    set year=20!year!
    set month=0!month!
    if "!month!" == "0A" set month=10
    if "!month!" == "0B" set month=11
    if "!month!" == "0C" set month=12
  )
  
  rem Compare date filename with current date
  rem If same day, check the ArchiveCurrDay value
  if "!year!!month!!day!" == "%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%" (
    if %DEBUG% == 1 echo [DEBUG] file: !filename! - file from current day
    if "!DATA[%ID_DATA%].ArchiveCurrDay!" == "0" (
      if %DEBUG% == 1 echo [DEBUG] !DATA[%ID_DATA%].ArchiveCurrDay! is set to 0, not archive ...
      set move=0
    )
  ) else (
    if %DEBUG% == 1 echo [DEBUG] file: !filename! - file from another day 
  )
  
  rem If tests are passed and move EQU 1, we move files in archives
  if !move! EQU 1 (
    if %DEBUG% == 1 echo [DEBUG] Entering in the "move" case ...
    if "%ArchiveFormat%" == "YYYY" (
      if %DEBUG% == 1 echo [DEBUG] ArchiveFormat "YYYY" defined
      if not exist !DATA[%ID_DATA%].ArchivesPath!\!year! (
	      if %DEBUG% == 1 echo [DEBUG] Creating the folder !DATA[%ID_DATA%].ArchivesPath!\!year!
	      md !DATA[%ID_DATA%].ArchivesPath!\!year! >NUL 2>&1
	    )
      if %DEBUG% == 1 echo [DEBUG] move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\
	    move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\ >NUL 2>&1
	    if %ERRORLEVEL% NEQ 0 exit /b %ERRORLEVEL%
    )
  
    if "%ArchiveFormat%" == "YYYYMM" (
      if %DEBUG% == 1 echo [DEBUG] ArchiveFormat "YYYYMM" defined
      if not exist !DATA[%ID_DATA%].ArchivesPath!\!year!\!month! (
	      if %DEBUG% == 1 echo [DEBUG] Creating the folder !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!
	      md !DATA[%ID_DATA%].ArchivesPath!\!year!\!month! >NUL 2>&1
	    )
      if %DEBUG% == 1 echo [DEBUG] move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\
	    move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\ >NUL 2>&1
	    if %ERRORLEVEL% NEQ 0 exit /b %ERRORLEVEL%
    )
  
    if "%ArchiveFormat%" == "YYYYMMDD" (
      if %DEBUG% == 1 echo [DEBUG] ArchiveFormat "YYYYMMDD" defined
      if not exist !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day! (
	      if %DEBUG% == 1 echo [DEBUG] Creating the folder !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!
	      md !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day! >NUL 2>&1
	    )
	    if %DEBUG% == 1 echo [DEBUG] move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!\
	    move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!\ >NUL 2>&1
	    if %ERRORLEVEL% NEQ 0 exit /b %ERRORLEVEL%
    )

    if "%ArchiveFormat%" == "YYMDD" (
      if %DEBUG% == 1 echo [DEBUG] ArchiveFormat "YYMDD" defined
      if not exist !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day! (
	      if %DEBUG% == 1 echo [DEBUG] Creating the folder !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!
	      md !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day! >NUL 2>&1
	    )
	    if %DEBUG% == 1 echo [DEBUG] move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!\
	    move !DATA[%ID_DATA%].DataPath!\"%%l" !DATA[%ID_DATA%].ArchivesPath!\!year!\!month!\!day!\ >NUL 2>&1
	    if %ERRORLEVEL% NEQ 0 exit /b %ERRORLEVEL%
    )
  ) else (
    if %DEBUG% == 1 echo [DEBUG] Skip the "move" case ...
  )
)
exit /b 0

:download
rem Download from the University of Lille GitLab repository
if %DEBUG% == 1 echo [DEBUG] curl "%URL_REPO%/%ARG3%/%ARG2%" -o %ARG4%\%ARG3%\%ARG2% >NUL 2>&1
curl "%URL_REPO%/%ARG3%/%ARG2%" -o %ARG4%\%ARG3%\%ARG2% >NUL 2>&1

rem Read the downloaded file
set /p file=<%ARG4%\%ARG3%\%ARG2%

rem Test its content (GitLab webpage when file not found)
if "!file:~10,4!" == "html" (
  del %ARG4%\%ARG3%\%ARG2% >NUL 2>&1
  exit /b 1
)
exit /b 0

:put
rem PUT
echo.
rem Set values received
set PATH_DATA=%ARG3%
set MASK_FILES=%ARG2%

rem Create a list with files ready to send (all)
echo Listing '%MASK_FILES%' files in "%PATH_DATA%" ...
if %DEBUG% == 1 echo [DEBUG] call %PATH_BIN%\%LIBRARY% list_files "%PATH_DATA%" %MASK_FILES%
call %PATH_BIN%\%LIBRARY% list_files "%PATH_DATA%" %MASK_FILES%
		
if !ERRORLEVEL! NEQ 0 (
	echo No input files found ...
	exit /b 1
) else (
	rem If %FTP_MAX% is > 0
	if %FTP_MAX% GTR 0 (
		rem Create a filtered list with files to send (see FTP_MAX limit)
		echo Preparing to send the %FTP_MAX% first files of this list ^(see FTP_MAX value in config file^)
		if %DEBUG% == 1 echo [DEBUG] call %PATH_BIN%\%LIBRARY% filter_list "%PATH_DATA%" %MASK_FILES%
		call %PATH_BIN%\%LIBRARY% filter_list "%PATH_DATA%" %MASK_FILES%
		if !ERRORLEVEL! NEQ 0 (
			echo No input files found ...
			exit /b 1
		)
	)
	rem Else if %FTP_MAX% EQU 0
  if %FTP_MAX% EQU 0 (
		move %PATH_TMP%\%ARG%.tmp %PATH_TMP%\%ARG%.txt >NUL 2>&1
	)
		
	if exist "%PATH_TMP%\%ARG%.txt" (
		rem Show files to send (read %ARG%.txt)
		for /f "delims=" %%l in (%PATH_TMP%\%ARG%.txt) do (
			echo %%l
		)
	)
)
exit /b 0

:sync
rem SYNC
echo.
rem Set values received
set MASK_FILES=%ARG2%
set PATH_DATA=%ARG3%
set PATH_REMOTE=%ARG4%
echo Preparing to synchronize %MASK_FILES% in "%PATH_DATA%" to %PATH_REMOTE% ...
exit /b 0

:common
rem COMMON
echo.
rem Set values received
set MASK_FILES=%ARG2%
set PATH_DATA=%ARG3%
set METHOD=%ARG4%
set ID_DATA=%ARG5%
set PATH_REMOTE=%ARG6%
set PATH_ARCH=%ARG7%

rem Create .wscript file
if %DEBUG% == 1 echo [DEBUG] call %PATH_BIN%\%LIBRARY% write_wscript "%PATH_DATA%" %METHOD% %MASK_FILES% %PATH_REMOTE% %ID_DATA%
call %PATH_BIN%\%LIBRARY% write_wscript "%PATH_DATA%" %METHOD% %MASK_FILES% %PATH_REMOTE% %ID_DATA%

rem Call WinSCP with .wscript file 
if %DEBUG% == 1 echo [DEBUG] call %PATH_BIN%\%LIBRARY% winscp
call %PATH_BIN%\%LIBRARY% winscp

rem If WINSCP fails, exit with error
if !ERRORLEVEL! NEQ 0 (
	echo.
	echo [ERROR] Problem during the transfer ^^!
	exit /b 1
) else (
	echo Transfer success ^^!
	echo %CURR_DATETIME% > %PATH_LOGS%\last_%ARG%_data%ID_DATA%.log
	rem If ArchivesPath not specified
  if "%PATH_ARCH%" == "" (
		if %DEBUG% == 1 echo [DEBUG] No value for DATA[%ID_DATA%].ArchivesPath
		if %DEBUG% == 1 echo [DEBUG] Leaving files in "%PATH_DATA%"
	) else ( 
		if %DEBUG% == 1 echo [DEBUG] DATA[%ID_DATA%].ArchivesPath = %PATH_ARCH%
		rem If folders not exist, create them
		if "%PATH_ARCH%" == "" (
			echo [ERROR] The directory %PATH_ARCH% doesn't exist ^^!
			echo Creating the directory %PATH_ARCH% ...
			md %PATH_ARCH% >NUL 2>&1
		)
		rem Archive uploaded files
		if %DEBUG% == 1 echo [DEBUG] call %PATH_BIN%\%LIBRARY% archive %ID_DATA%
		call %PATH_BIN%\%LIBRARY% archive %ID_DATA%
		if !ERRORLEVEL! NEQ 0 (
			echo [ERROR] Unable to move files in the archives directory ^^!
			exit /b 1
		)
	)
)
exit /b 0
